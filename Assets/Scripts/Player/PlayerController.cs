using System;
using Singleton;
using Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerController : MonoSingleton<PlayerController>
    {
        [SerializeField] public PlayerSpaceship playerSpaceship;
        private Vector2 _movementInput = Vector2.zero;
        private ShipInputActions _inputActions;
        private float _minX;
        private float _maxX;
        private float _minY;
        private float _maxY;

        private void Awake()
        {
            InnerInput();
            CreateMovementBoundary();
        }

        

        private void OnFire(InputAction.CallbackContext obj)
        {
            playerSpaceship.OnFire();
        } 
        private void InnerInput()
        {
            _inputActions = new ShipInputActions();
            _inputActions.Player.Move.performed += OnMove;
            _inputActions.Player.Move.canceled += OnMove;
            _inputActions.Player.Fire.performed += OnFire;
        }
        private void Update()
                  {
                      Move();
                  }
                  
                  private void CreateMovementBoundary()
                  {
                      Camera	 mainCamera = Camera.main;
                      Debug.Assert(mainCamera != null, "mainCamera cannot be null");
          
                      SpriteRenderer spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
                      Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
          
                      Vector3 offset = spriteRenderer.bounds.size;
                      _minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
                      _maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x + offset.x / 2;
                      _minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
                      _maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y + offset.y / 2;
                  }
        private void Move()
        {
            Vector2 inputVelocity = _movementInput * playerSpaceship.Speed;

            Vector3 position = transform.position;
            Vector3 newPosition = position;
            newPosition.x = position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = position.y + inputVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, _minX, _maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, _minY, _maxY);

            position = newPosition;
            transform.position = position;
        }
        
        public void OnMove(InputAction.CallbackContext obj)
        {
            if (obj.performed)
            {
                _movementInput = obj.ReadValue<Vector2>();
            }

            if (obj.canceled)
            {
                _movementInput = Vector2.zero;
            }
        }

        private void OnEnable()
        {
            _inputActions.Enable();
        }

        private void OnDisable()
        {
            _inputActions.Disable();
        }
    }
}
