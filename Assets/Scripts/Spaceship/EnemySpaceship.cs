using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, GetDamagable
    {
        
        public event Action onDestroy;

        public void Inner(int hp, float speed)
        {
            base.Inner(hp, speed, playerBullet);
        }
        
        public void GetHit(int damage)
        {
            Hp -= damage;
            
            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }
        
        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP more than zero");
            GameObject o;
            (o = gameObject).SetActive(false);

            Destroy(o);
            onDestroy?.Invoke();
            
        }
        
        public override void OnFire()
        { 
        }
    }
}
