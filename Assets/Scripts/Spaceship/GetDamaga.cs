﻿using System;

namespace Spaceship
{
    public interface GetDamagable
    {
        event Action onDestroy;
        void GetHit(int damage);
        void Explode();
    }
}