using UnityEngine;
using UnityEngine.Serialization;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        [FormerlySerializedAs("defBullet")] [SerializeField] protected Bullet playerBullet;
        [SerializeField] protected Transform gunPosition;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        protected void Inner(int hp, float speed, Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }

        public abstract void OnFire();
    }
}
