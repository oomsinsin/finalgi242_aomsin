using System;
using UnityEngine;
using static UnityEngine.Debug;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, GetDamagable
    {
        public event Action onDestroy;
        
        private void Awake()
        {
            Assert(playerBullet != null, "defaultBullet cannot be null");
            Assert(gunPosition != null, "gunPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Inner(hp, speed, playerBullet);
        }
        
        public override void OnFire()
        {
            Bullet bullet;
            bullet = Instantiate(playerBullet, gunPosition.position, Quaternion.identity);
            bullet.Inner();
        }
        public void Explode()
        {
            Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject);
            if (onDestroy != null) onDestroy?.Invoke();
        }
        
        public void GetHit(int damage)
        {
            Hp = Hp - damage;
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        
    }
}
