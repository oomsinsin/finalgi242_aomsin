using UnityEngine;
using UnityEngine.Serialization;
using static UnityEngine.Debug;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    { 
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [FormerlySerializedAs("rigidbody2D")] [SerializeField] private new Rigidbody2D rigid2D; 
        
        private void Move()
        {
            rigid2D.velocity = Vector2.up * speed;
        }
        
        public void Inner()
        {
            Move();
        }

        private void Awake()
        {
            Assert(rigid2D != null, "rigidbody2D cant be null");
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            GetDamagable target;
            target = other.gameObject.GetComponent<GetDamagable>();
            target?.GetHit(damage);
        }
    }
}
