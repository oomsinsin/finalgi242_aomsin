using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [FormerlySerializedAs("")] [SerializeField] private TextMeshProUGUI scoreUI;
        [FormerlySerializedAs("")] [FormerlySerializedAs("")] [SerializeField] private TextMeshProUGUI summaryScoreUI;
        
        public void Inner()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            HideSummaryScore(true);
            SetScore(0);
            SetSummaryScore(0);
        }
        
        private void Awake()
        {
            Debug.Assert(scoreUI != null, "scoreText cant null");
            Debug.Assert(summaryScoreUI != null, "summaryScoreText cant null");
        } 
        
        private void HideScore(bool onHide)
        {
            scoreUI.gameObject.SetActive(!onHide);
        }
        
        private void HideSummaryScore(bool onHide)
        {
            summaryScoreUI.gameObject.SetActive(!onHide);
        }
        
        private void OnRestarted()
        {
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(false);
            HideSummaryScore(false);
        } 
        
        public void SetScore(int score)
        {
            scoreUI.text = string.Format("Score : {0}", score);
        }
        
        public void SetSummaryScore(int score)
        {
            summaryScoreUI.text = string.Format("SummaryScore : {0}", score);
        }
    }
}
