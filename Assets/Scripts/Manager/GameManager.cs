using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startClickButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitClickButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private ScoreManager scoreManager;
        
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemyShipSpeed;
                
        public static GameManager Instance { get; private set; }

        public event Action OnRestarted;

        private void Awake()
        {
            if (Instance==null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            startClickButton.onClick.AddListener(StartButton);
            quitClickButton.onClick.AddListener(QuitButton);
        }

        private void StartButton()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }
        
        private static void QuitButton()
        {
            Application.Quit();
        }

        private static void GetRestartButton()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
        private void StartGame()
        { 
            SpawnPlayer();
            SpawnEnemy();
            scoreManager.Inner();
           
        }
        
        private void OnPlayerSpaceshipExploded()
        {
            GameRestart();
        }
        
        private void SpawnPlayer()
        { 
            PlayerSpaceship spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.onDestroy += OnPlayerSpaceshipExploded;
        }

        private void SpawnEnemy()
        {
            EnemySpaceship spaceship = Instantiate(enemySpaceship);
            spaceship.Inner(enemySpaceshipHp, enemyShipSpeed);
            spaceship.onDestroy += EnemyDestroy;
        } 
        
        private void GameRestart()
        {
            restartButton.onClick.AddListener(GetRestartButton);
            OnRestarted?.Invoke();
        }
        
        private void EnemyDestroy()
        {
            scoreManager.SetScore(+1);
            scoreManager.SetSummaryScore(+1);
            GameRestart();
        }
        
        
    }
}
